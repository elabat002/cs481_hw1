import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Welcome to Flutter',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Homework 1'),
        ),
        body: Center(
          child: Text("""
          🙋‍♂ Enzo LABAT ️- 20 years old 
          🎓 Gradudated in 2022
          🗣️ "J'aime le bruit blanc de l'eau." - OSS 117
          """),
        ),
      ),
    );
  }
}